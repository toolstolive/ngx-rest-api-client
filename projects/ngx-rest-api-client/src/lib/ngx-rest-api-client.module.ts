import { ModuleWithProviders, NgModule } from '@angular/core';
import { ErrorHandlerService } from './services/error-handler.service';
import { HttpParamsFactoryService } from './services/http-params-factory.service';
import { RequestHandlerService } from './services/request-handler.service';
import { RestApiHttpClientService } from './services/rest-api-http-client.service';
import { RouteBuilderService } from './services/route-builder.service';

@NgModule()
export class NgxRestApiClientModule {
  static forRoot(): ModuleWithProviders<NgxRestApiClientModule> {
    return {
      ngModule: NgxRestApiClientModule,
      providers: [
        RouteBuilderService,
        HttpParamsFactoryService,
        RestApiHttpClientService,
        RequestHandlerService,
        ErrorHandlerService,
      ],
    };
  }
}
