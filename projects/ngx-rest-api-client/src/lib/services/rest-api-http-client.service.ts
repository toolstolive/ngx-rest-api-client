import { Inject, Injectable, Optional } from '@angular/core';
import { HttpBackend, HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable, Subscription } from 'rxjs';
import { AuthTokenLoader, AUTH_TOKEN_LOADER_TOKEN } from '../model/injection-tokens/auth-token-loader-token';

@Injectable()
export class RestApiHttpClientService {
  private http: HttpClient;

  constructor(
    httpBackend: HttpBackend,
    @Optional() @Inject(AUTH_TOKEN_LOADER_TOKEN) private tokenLoader?: AuthTokenLoader
  ) {
    this.http = new HttpClient(httpBackend);
  }

  get<T>(url: string, options?: ApiHttpRequestOptions): Observable<HttpResponse<T>> {
    return this.request('get', url, null, options);
  }

  post<T>(url: string, body: any | null, options?: ApiHttpRequestOptions): Observable<HttpResponse<T>> {
    return this.request('post', url, body, options);
  }

  put<T>(url: string, body: any | null, options?: ApiHttpRequestOptions): Observable<HttpResponse<T>> {
    return this.request('put', url, body, options);
  }

  delete<T>(url: string, options?: ApiHttpRequestOptions): Observable<HttpResponse<T>> {
    return this.request('delete', url, null, options);
  }

  private request<T>(method: string, url: string, body: any | null, options?: ApiHttpRequestOptions): Observable<HttpResponse<T>> {
    const obs = new Observable<HttpResponse<T>>(s => {
      let httpSubscription: Subscription;
      const optSubscription = this.checkOptions(options).subscribe(opt => {
        // tslint:disable-next-line: max-line-length
        const request = this.http.request<T>(method, url, { body, headers: opt.headers, observe: 'response', params: opt.params, reportProgress: false, responseType: opt.responseType, withCredentials: true });
        httpSubscription = request.subscribe(next => {
          s.next(next);
        }, error => {
          s.error(error);
        }, () => {
          s.complete();
        });
      });

      return () => {
        optSubscription.unsubscribe();
        if (httpSubscription) {
          httpSubscription.unsubscribe();
        }
      };
    });

    return obs;
  }

  private checkOptions(options?: ApiHttpRequestOptions): Observable<ApiHttpRequestOptions> {
    if (!options) {
      options = {};
    }
    if (!options.responseType) {
      options.responseType = 'json';
    }

    return new Observable(subscribe => {
      if (!this.tokenLoader) {
        subscribe.next(options);
        subscribe.complete();
        return;
      }

      this.tokenLoader.getTokenAsync()
        .then((authToken: string | null) => {
          if (authToken && options !== undefined) {
            options.headers = this.appendHeader(options.headers, 'Authorization', `Bearer ${authToken}`);
          }
          subscribe.next(options);
        }).catch((error) => {
          subscribe.next(options);
          console.error('cant load auth token', error);
        }).finally(() => {
          subscribe.complete();
        });

      // return () => {
        // unsubscribe logic
      // };
    });
  }

  private appendHeader(headers: HttpHeaders | undefined, name: string, value: string): HttpHeaders {
    if (!headers) {
      headers = new HttpHeaders();
    }
    return headers.append(name, value);
  }
}

export interface ApiHttpRequestOptions {
  headers?: HttpHeaders;
  params?: HttpParams;
  responseType?: 'json';
}
