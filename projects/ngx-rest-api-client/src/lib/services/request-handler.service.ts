import { Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { ErrorHandlerService } from './error-handler.service';
import { Injectable } from '@angular/core';
import { ApiRequest } from '../model/api-request';
import { RequestCancellationToken } from '../model/request-cancellation-token';

@Injectable()
export class RequestHandlerService {

constructor(
  private errorHandler: ErrorHandlerService,
) { }

public handle<T>(request: Observable<HttpResponse<T>>, parser?: (res: T) => void): ApiRequest<T> {
  const cancellationToken = new RequestCancellationToken();

  var result = new Promise<T | null>((resolve, reject) => {
    request
      .pipe(takeUntil(cancellationToken.unsubscriber)) // Обеспечивает возможность отмены запроса
      .subscribe({
        next: (response: HttpResponse<T>) => {
          const data = response.body;
          if (data) {
            if (parser) {
              try {
                parser(data);
              } catch (error) {
                console.error(error);
              }
            }
          }
          resolve(data);
        },
        error: (error: HttpErrorResponse) => {
          const apiError = this.errorHandler.handle(error);
          reject(apiError);
        }
      });
    });
  return { result, cancellationToken}
}
}
