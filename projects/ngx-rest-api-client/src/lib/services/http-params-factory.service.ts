import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class HttpParamsFactoryService {

  constructor() { }

  create(requestParams: any): HttpParams {
    const params = this.parseObject(requestParams, new HttpParams());
    return params;
  }

  private parseObject(requestParams: any, params: HttpParams): HttpParams {
    for (const key in requestParams) {
      if (Object.prototype.hasOwnProperty.call(requestParams, key)) {
        let value = requestParams[key];
        if (value === undefined || value == null) {
          continue;
        }
        if (typeof value === 'object') {
          params = this.parseObject(value, params); // parse recursively if it is an object
        } else {
          if (typeof value !== 'string') {
            value = value.toString();
          }
          params = params.set(key, value);
        }
      }
    }
    return params;
  }
}
