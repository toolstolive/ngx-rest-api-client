import { HttpErrorResponse } from '@angular/common/http';
import { Inject, Injectable, Optional } from '@angular/core';
import { ApiRequestError } from '../model/api-request-error';
import { API_REQUEST_ERROR_HANDLER_TOKEN, ApiRequestErrorHandler } from '../model/injection-tokens/api-request-error-handler-token';

@Injectable()
export class ErrorHandlerService {

  constructor(
    @Optional() @Inject(API_REQUEST_ERROR_HANDLER_TOKEN) private customErrorHandler?: ApiRequestErrorHandler
  ) { }

  async handle(error: HttpErrorResponse): Promise<ApiRequestError> {
    console.warn(error.message);
    console.log(error);

    const apiError: ApiRequestError = { message: error?.message, httpErrorResponse: error };
    let customError: ApiRequestError | null = null;

    if (this.customErrorHandler) {
      customError = await this.customErrorHandler.handle(apiError);
    }

    return customError ?? apiError;
  }
}
