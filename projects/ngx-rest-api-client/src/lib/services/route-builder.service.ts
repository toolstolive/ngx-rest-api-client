import { Injectable } from '@angular/core';

@Injectable()
export class RouteBuilderService {

constructor(
) {}

  public build(baseUrl: string, relUrl: string, ...routes: string[]): string {
    let url = `${baseUrl}/${relUrl}`;
    if (routes && routes.length > 0) {
      routes.forEach(route => {
        url += `/${route}`;
      });
    }
    return url;
  }
}
