export interface PaginationPage<T> {
  page: Array<T>;
  totalCount: number;
}
