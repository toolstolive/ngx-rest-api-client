import { RequestCancellationToken } from './request-cancellation-token';

export interface ApiRequest<T> {
  result: Promise<T | null>;
  cancellationToken: RequestCancellationToken;
}
