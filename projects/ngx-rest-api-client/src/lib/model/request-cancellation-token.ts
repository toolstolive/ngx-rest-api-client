import { ReplaySubject } from 'rxjs';

export class RequestCancellationToken {
  public isCanceled = false;
  public unsubscriber = new ReplaySubject<void>(1);

  cancel(): void {
    if (!this.isCanceled) {
      this.isCanceled = true;
      this.unsubscriber.next();
      this.unsubscriber.complete();
    }
  }
}

export function RequestCancellationTokenFactory(): RequestCancellationToken {
  return new RequestCancellationToken();
}
