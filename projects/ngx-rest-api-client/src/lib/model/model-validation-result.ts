export interface ModelValidationResult {
    errors: ModelValidationError[];
    isValid: boolean;
}

export interface ModelValidationError {
    fieldName: string;
    errorMessages: string[];
}
