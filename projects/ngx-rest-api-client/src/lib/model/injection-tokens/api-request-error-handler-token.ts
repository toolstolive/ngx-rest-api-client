import { InjectionToken } from '@angular/core';
import { ApiRequestError } from '../api-request-error';
export interface ApiRequestErrorHandler {
    handle(apiRequestError: ApiRequestError): Promise<ApiRequestError>;
}
export const API_REQUEST_ERROR_HANDLER_TOKEN = new InjectionToken<ApiRequestErrorHandler>('ApiRequestErrorHandler');
