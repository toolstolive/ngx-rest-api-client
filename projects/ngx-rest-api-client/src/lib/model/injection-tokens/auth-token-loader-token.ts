import { InjectionToken } from '@angular/core';
export interface AuthTokenLoader {
    getTokenAsync(): Promise<string | null>;
}
export const AUTH_TOKEN_LOADER_TOKEN = new InjectionToken<AuthTokenLoader>('AuthTokenLoader');
