import { ModelValidationResult } from "./model-validation-result";

export interface AjaxResult<T> {
  isSuccess: boolean;
  resultType: AjaxResultType;
  message?: string;
  resultObject?: T | ModelValidationResult;
}

export enum AjaxResultType {
  Fail = "Fail",
  Success = "Success",
  ValidationError = "ValidationError"
}
