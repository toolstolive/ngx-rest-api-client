import { HttpErrorResponse } from '@angular/common/http';


export interface ApiRequestError {
  message?: string;
  httpErrorResponse?: HttpErrorResponse;
}
