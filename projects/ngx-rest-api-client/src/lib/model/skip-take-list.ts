export interface SkipTakeList<T> {
  list: Array<T>;
  totalCount: number;
}
